import React from 'react';
import { BrowserRouter, Route ,Redirect  } from 'react-router-dom';
import UserRedux from './userRedux';
import TodoRedux from './todoRedux';
import PrivateRoute from './privateRoute';
import Login from './loginRedux';
import { useDispatch } from 'react-redux';
import { login, logout } from '../reducers/loginReducer'

const Main = () => {
    const dispatch = useDispatch();
    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                dispatch(login())
                return <Redirect to="/users" />
            }}></Route>
            <Route path="/processLogout" render={() => {
                dispatch(logout())
                return <Redirect to="/" />
            }}></Route>

            <Route path="/" component={Login} exact={true}></Route>
            <PrivateRoute  path="/users" component={UserRedux} exact={true}></PrivateRoute>
            <PrivateRoute path="/users/:user_id/todo" component={TodoRedux}></PrivateRoute>
        </BrowserRouter>
    )
}
export default Main;