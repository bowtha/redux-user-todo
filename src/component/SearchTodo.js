import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { Input } from 'antd';
import '../css/userPage.css';
import 'antd/dist/antd.css';
import { searchTodo } from '../reducers/todoReducer';

const { Search } = Input;

const SearchTodo = () => {

    const dispatch = useDispatch()

    const setSearchText = (text) => {
        dispatch(searchTodo(text))
    }

    return (
        <div>
            <Search
                placeholder="input search text"
                onChange={(e) => setSearchText(e.target.value)}
            />
        </div>
    )
}

export default SearchTodo;