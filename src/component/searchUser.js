import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { Input } from 'antd';
import '../css/userPage.css';
import 'antd/dist/antd.css';
import { searchName } from '../actions/userActions';

const { Search } = Input;

const SearchUser = () => {

    const dispatch = useDispatch()

    const setSearchText = (name) => {
        dispatch(searchName(name))
    }

    return (
        <div>
            <Search
                placeholder="input search name"
                onChange={(e) => setSearchText(e.target.value)}
                style={{ width: 200 }}
            />
        </div>
    )
}

export default SearchUser;