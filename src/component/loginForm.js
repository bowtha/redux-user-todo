import React from 'react';
import { Form,  Input, Button } from 'antd';
import '../css/login.css'
class LoginForm extends React.Component {

    
    render() {

        return (
            <Form className="login-form">
                <Form.Item>
                    <Input
                        placeholder="Username"
                    />
                </Form.Item>
                <Form.Item>
                    <Input
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <a className="login-form-forgot" href="">Forgot password</a>
                    <Button type="primary" htmlType="submit" className="login-form-button" onClick={() => {window.location = "/processLogin"}}>Login</Button>
                    <a href="">register now!</a>
                </Form.Item>
            </Form>
        );
    }
}


export default LoginForm;
