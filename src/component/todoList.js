import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { List, Icon, Select, Button } from 'antd';
import 'antd/dist/antd.css';
import '../css/todoPage.css'
import { filterStatus, changStatus } from '../reducers/todoReducer'
const { Option } = Select;

const TodoList = () => {

    const todos = useSelector(state => state.todosCompState.todos)
    const filter = useSelector(state => state.todosCompState.filter)
    const text = useSelector(state => state.todosCompState.searchText)

    const dispatch = useDispatch();

    const handleChange = (value) => {
        dispatch(filterStatus(value))
    }
    const changeStatusTodo = (index) => {
        dispatch(changStatus(index))
    }


    let renderData = todos;
    renderData = filter == -1 ?
        renderData
        :
        renderData.filter(data => data.completed == filter);

    renderData = text == '' ?
        renderData
        :
        renderData.filter(data => (data.title.match(text)));



    return (
        <div>

            <div className="demo-infinite-container">

                <Select defaultValue="Status" style={{ width: 120 }} onChange={handleChange}>
                    <Option value={-1}>All</Option>
                    <Option value={true}>Done</Option>
                    <Option value={false}>Doing</Option>
                </Select>

                <List
                    dataSource={renderData}
                    renderItem={item => (
                        <List.Item key={item.id}>

                            <List.Item.Meta
                                title={<a ><Icon type="right" />{item.id}</a>}
                                description={item.title}
                            />
                            {
                                item.completed ?
                                    <Button type="dashed">
                                        Done
                                        </Button>
                                    :
                                    <Button type="primary" onClick={() => changeStatusTodo(item.id)}>
                                        Doing
                                        </Button>
                            }
                        </List.Item>
                    )}
                >
                </List>
            </div>
        </div>
    )
}

export default TodoList;