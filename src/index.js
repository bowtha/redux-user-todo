import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore , applyMiddleware, compose} from 'redux';
import { rootReducer } from './reducers';
import { Provider } from 'react-redux';
import Main from './component/main';
import thunk from 'redux-thunk'



const composeEnhancers =
    typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(thunk),
);

const appStore = createStore(rootReducer, enhancer)

ReactDOM.render(
    <Provider store={appStore}>
        <Main />
    </Provider>
    , document.getElementById('root'));

serviceWorker.unregister();