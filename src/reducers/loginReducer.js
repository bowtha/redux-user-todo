const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';

export const login = () => {
    localStorage.setItem("isLogin", true)
    return {
        type: LOGIN
    }
}
export const logout = () => {
    localStorage.setItem("isLogin", false)
    return {
        type: LOGOUT
    }
}



const initialState = {

    todos: [],
    isLogin: false,

}


export const Login = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                isLogin: true
            }

        case LOGOUT:
            return {
                ...state,
                isLogin: false
            }
            
        default:
            return state
    }
}

