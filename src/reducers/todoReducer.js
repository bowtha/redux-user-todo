const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN';
const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
const FILTER_TODO = 'FILTER_TODO';
const CHANGE_STATUS = 'CHANGE_STATUS';
const SEARCH_TODO = 'SEARCH_TODO';


export const fetchTodo = (userID) => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userID)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODO_BEGIN
    }
}

export const fetchTodoSuccess = (todos) => {
    return {
        type: FETCH_TODO_SUCCESS,
        payLoad: todos
    }
}

export const fetchTodoError = (error) => {
    return {
        type: FETCH_TODO_ERROR,
        payLoad: error
    }
}

export const filterStatus = (value) => {
    return {
        type: FILTER_TODO,
        payLoad: value
    }
}
export const changStatus = (index) => {
    return {
        type: CHANGE_STATUS,
        payLoad: index
    }
}

export const searchTodo = (text) => {
    return {
        type: SEARCH_TODO,
        payLoad: text
    }
}

const initialState = {

    todos: [],
    loading: false,
    error: '',
    filter: -1,
    searchText: ''

}


export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TODO_BEGIN:
            return {
                ...state,
                loading: true
            }

        case FETCH_TODO_SUCCESS:
            return {
                todos: action.payLoad,
                loading: false,
                error: '',
                filter: -1
            }

        case FETCH_TODO_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        case FILTER_TODO:
            return {
                ...state,
                filter: action.payLoad
            }
        case CHANGE_STATUS:
            let updateTodo = state.todos.find(m => m.id == action.payLoad)
            updateTodo.completed = true
            return {
                ...state,
                todos: [
                    ...state.todos,
                    updateTodo
                ]
            }
        case SEARCH_TODO:
            return {
                ...state,
                searchText: action.payLoad
            }
        default:
            return state
    }
}

