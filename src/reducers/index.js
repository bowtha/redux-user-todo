import { combineReducers } from 'redux';
import { userReducer } from './userReducer';
import { todoReducer } from './todoReducer';


// const rootReducer = combineReducers({
//     combineReducers
// })

export const rootReducer = combineReducers({
    users : userReducer,
    todosCompState : todoReducer
})